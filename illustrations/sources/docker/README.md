This folder defines the Docker image used by the [Makefile](../../../Makefile) to build the [illustrations](../../) from their [sources](../).
