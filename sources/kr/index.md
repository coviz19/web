---
title: 한국
id: kr
---

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=173213315&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=173213315&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=173213315&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=168932019&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=168932019&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=168932019&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1626783066&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1626783066&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1626783066&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1632821359&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1632821359&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1632821359&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1379752170&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1379752170&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1379752170&amp;format=image">PNG</a></figcaption>
</figure>

# Sources

* [Korean Center for Disease Control — Press Release](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030)
  - [2020-03-26 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366663&tag=&nPage=1)
  - [2020-03-25 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366636&tag=&nPage=1)
  - [2020-03-24 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366646&tag=&nPage=1)
  - [2020-03-23 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366631&tag=&nPage=1)
  - [2020-03-22 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366623&tag=&nPage=1)
  - [2020-03-21 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366618&tag=&nPage=1)
  - [2020-03-20 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366615&tag=&nPage=1)
  - [2020-03-19 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366606&tag=&nPage=1)
  - [2020-03-18 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366590&tag=&nPage=1)
  - [2020-03-17 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366583&tag=&nPage=1)
  - [2020-03-16 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366573&tag=&nPage=1)
  - [2020-03-15 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366565&tag=&nPage=1)
  - [2020-03-14 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366555&tag=&nPage=1)
  - [2020-03-13 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366553&tag=&nPage=1)
  - [2020-03-12 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366546&tag=&nPage=1)
  - [2020-03-11 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366537&tag=&nPage=1)
  - [2020-03-10 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366515&tag=&nPage=1)
  - [2020-03-09 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366513&tag=&nPage=1)
  - [2020-03-08 24:00](https://www.cdc.go.kr/board/board.es?mid=a30402000000&bid=0030&act=view&list_no=366493&tag=&nPage=1)
  - …
* [Wikipedia — 2020 coronavirus pandemic in South Korea](https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_South_Korea)
* [Wikipedia — 2019–20 coronavirus pandemic data](https://en.wikipedia.org/wiki/Template:2019%E2%80%9320_coronavirus_pandemic_data)
* [Wikipedia — COVID-19 testing](https://en.wikipedia.org/wiki/Template:COVID-19_testing)
