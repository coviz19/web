---
title: Deutschland
id: de
---

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1779305751&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1779305751&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1779305751&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=938968623&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=938968623&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=938968623&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1149080835&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1149080835&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1149080835&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1852948276&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1852948276&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1852948276&amp;format=image">PNG</a></figcaption>
</figure>

# Sources

* [Berliner Morgenpost —— Coronavirus Monitor](https://interaktiv.morgenpost.de/corona-virus-karte-infektionen-deutschland-weltweit/)
* [Robert Koch Institut — COVID-19: Fallzahlen in Deutschland und weltweit](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Fallzahlen.html?nn=13490888)
* [Robert Koch Institut — Archiv der Situationsberichte des Robert Koch-Instituts zu COVID-19](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/Archiv.html)
  - [2020-04-02 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-04-02-en.pdf?__blob=publicationFile) (First official test numbers)
  - [2020-04-01 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-04-01-en.pdf?__blob=publicationFile)
  - [2020-03-31 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-31-en.pdf?__blob=publicationFile)
  - [2020-03-30 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-30-en.pdf?__blob=publicationFile)
  - [2020-03-29 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-29-en.pdf?__blob=publicationFile)
  - [2020-03-28 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-28-en.pdf?__blob=publicationFile)
  - [2020-03-27 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-27-en.pdf?__blob=publicationFile)
  - [2020-03-26 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-26-en.pdf?__blob=publicationFile)
  - [2020-03-25 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-25-en.pdf?__blob=publicationFile)
  - [2020-03-24 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-24-en.pdf?__blob=publicationFile)
  - [2020-03-23 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-23-en.pdf?__blob=publicationFile)
  - [2020-03-22 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-22-en.pdf?__blob=publicationFile)
  - [2020-03-21 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-21-en.pdf?__blob=publicationFile)
  - [2020-03-20 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-20-en.pdf?__blob=publicationFile)
  - [2020-03-19 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-19-en.pdf?__blob=publicationFile)
  - [2020-03-18 00:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-18-en.pdf?__blob=publicationFile)
  - [2020-03-17 15:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-17-en.pdf?__blob=publicationFile)
  - [2020-03-16 15:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-16-en.pdf?__blob=publicationFile)
  - [2020-03-15 15:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-15-en.pdf?__blob=publicationFile)
  - [2020-03-14 15:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-14-en.pdf?__blob=publicationFile)
  - [2020-03-13 15:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-13-en.pdf?__blob=publicationFile)
  - [2020-03-12 15:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-12-en.pdf?__blob=publicationFile)
  - [2020-03-11 15:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-11-en.pdf?__blob=publicationFile)
  - [2020-03-10 14:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-10-en.pdf?__blob=publicationFile)
  - [2020-03-09 14:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-09-en.pdf?__blob=publicationFile)
  - [2020-03-08 15:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-08-en.pdf?__blob=publicationFile)
  - [2020-03-07 15:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-07-en.pdf?__blob=publicationFile)
  - [2020-03-06 15:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-06-en.pdf?__blob=publicationFile)
  - [2020-03-05 15:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-05-en.pdf?__blob=publicationFile)
  - [2020-03-04 15:00](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/2020-03-04-en.pdf?__blob=publicationFile)
* [Wikipedia — 2020 coronavirus pandemic in Germany](https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_Germany)
* [Wikipedia — 2019–20 coronavirus pandemic data](https://en.wikipedia.org/wiki/Template:2019%E2%80%9320_coronavirus_pandemic_data)
* [Wikipedia — COVID-19 testing](https://en.wikipedia.org/wiki/Template:COVID-19_testing)
