---
title: España
id: es
---

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1299130498&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1299130498&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1299130498&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1783680250&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1783680250&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1783680250&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=131354248&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=131354248&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=131354248&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=630535414&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=630535414&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=630535414&amp;format=image">PNG</a></figcaption>
</figure>

# Sources

* [Salud Pública — Situación actual](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/situacionActual.htm)
  - [2020-03-27 21:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_57_COVID-19.pdf)
  - [2020-03-26 21:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_57_COVID-19.pdf)
  - [2020-03-25 21:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_56_COVID-19.pdf)
  - [2020-03-24 21:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_55_COVID-19.pdf)
  - [2020-03-23 21:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_54_COVID-19.pdf)
  - [2020-03-22 21:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_53_COVID-19.pdf)
  - [2020-03-21 21:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_52_COVID-19.pdf)
  - [2020-03-20 21:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_51_COVID-19.pdf)
  - [2020-03-19 21:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_50_COVID-19.pdf)
  - [2020-03-18 21:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_49_COVID-19.pdf)
  - [2020-03-17 21:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_48_COVID-19.pdf)
  - [2020-03-16 21:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_47_COVID-19.pdf)
  - [2020-03-16 13:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_46_COVID-19.pdf)
  - [2020-03-13 13:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_43_COVID-19.pdf)
  - [2020-03-12 13:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_42_COVID-19.pdf)
  - [2020-03-11 13:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_41_COVID-19.pdf)
  - [2020-03-10 13:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_40_COVID-19.pdf)
  - [2020-03-09 13:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_39_COVID-19.pdf)
  - [2020-03-06 13:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_38_COVID-19.pdf)
  - [2020-03-05 13:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_37_COVID-19.pdf)
  - [2020-03-04 13:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_36_COVID-19.pdf)
  - [2020-03-03 13:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_35_COVID-19.pdf)
  - [2020-03-02 13:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_34_COVID-19.pdf)
  - [2020-02-28 13:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_33_COVID-19.pdf)
  - [2020-02-27 13:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_32_COVID-19.pdf)
  - [2020-02-26 13:00](https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_31_COVID-19.pdf)
  - …
* [Wikipedia — 2020 coronavirus pandemic in Spain](https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_Spain)
* [Wikipedia — 2019–20 coronavirus pandemic data](https://en.wikipedia.org/wiki/Template:2019%E2%80%9320_coronavirus_pandemic_data)
* [Wikipedia — COVID-19 testing](https://en.wikipedia.org/wiki/Template:COVID-19_testing)
