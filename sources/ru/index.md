---
title: Россия
id: ru
---

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1702557167&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1702557167&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1702557167&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=220414816&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=220414816&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=220414816&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1804998586&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1804998586&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1804998586&amp;format=image">PNG</a></figcaption>
</figure>

# Sources

* [Wikipedia — 2020 coronavirus pandemic in Russia](https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_Russia)
* [Wikipedia — 2019–20 coronavirus pandemic data](https://en.wikipedia.org/wiki/Template:2019%E2%80%9320_coronavirus_pandemic_data)
* [Wikipedia — COVID-19 testing](https://en.wikipedia.org/wiki/Template:COVID-19_testing)
