---
title: Italia
id: it
---

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=818148970&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=818148970&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=818148970&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=714641728&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=714641728&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=714641728&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=2146309036&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=2146309036&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=2146309036&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1835876711&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1835876711&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=1835876711&amp;format=image">PNG</a></figcaption>
</figure>

<figure>
	<img class= "google-sheets image" width="800px" height="400px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=339868804&amp;format=image" />
	<iframe class="google-sheets interactive" width="800" height="400" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=339868804&amp;format=interactive"></iframe>
	<figcaption><a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnQ3a4tNFCN_oebTCyv8M3TaX3cfujhp-ODNJ007SPMygAxk2MA_SQrovuSUCu4IZ-6TQhWNFoU-5v/pubchart?oid=339868804&amp;format=image">PNG</a></figcaption>
</figure>

# Sources

* [GitHub — Protezione Civile — COVID-19](https://github.com/pcm-dpc/COVID-19)
  - [Daily CSV reports](https://github.com/pcm-dpc/COVID-19/tree/master/dati-andamento-nazionale)
  - [Daily PDF reports](https://github.com/pcm-dpc/COVID-19/tree/master/schede-riepilogative/regioni)
  - [JSON data](https://github.com/pcm-dpc/COVID-19/tree/master/dati-json)
* [Wikipedia — 2020 coronavirus pandemic in Italy](https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_Italy)
* [Wikipedia — 2019–20 coronavirus pandemic data](https://en.wikipedia.org/wiki/Template:2019%E2%80%9320_coronavirus_pandemic_data)
* [Wikipedia — COVID-19 testing](https://en.wikipedia.org/wiki/Template:COVID-19_testing)
