SOURCES_DIR := sources
OUTPUT_DIR := html5
TEMPLATE_DIR := template

.PHONY: $(OUTPUT_DIR)
$(OUTPUT_DIR):
	for publication in `find $(SOURCES_DIR) -name *.md | cut -d/ -f2- | rev | cut -c 4- | rev`; do \
		mkdir -p $(OUTPUT_DIR)/$$(dirname $${publication}) ; \
		pandoc --read markdown_github+yaml_metadata_block+footnotes+implicit_figures+inline_notes \
		       --write html5 \
		       --template $(TEMPLATE_DIR)/template.html \
		       --include-in-header $(TEMPLATE_DIR)/template.css \
		       --output $(OUTPUT_DIR)/$${publication}.html \
		       $(SOURCES_DIR)/$$publication.md ; \
	done

.PHONY: illustrations
illustrations:
	docker run --rm \
	           --volume `pwd`/illustrations/sources:/home/sources \
	           --volume `pwd`/illustrations:/home/illustrations:rw \
	           --user $(shell id --user):$(shell id --group) \
	           `docker build illustrations/sources/docker/ --quiet` \
	           make

.PHONY: monitor
monitor:
	while inotifywait -r -e modify .; do make illustrations && make html5; done

.phony: clean veryclean
clean:
	rm -f $(OUTPUT_DIR)/*
veryclean: clean
	rm -f illustrations/*
